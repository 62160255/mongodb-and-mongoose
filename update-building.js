const mongoose = require('mongoose')
const Room = require('./model/Room')
const Building = require('./model/Building')
mongoose.connect('mongodb://localhost:27017/example')

async function main () {
  const newInformaticBuilding = await Building.findById('621bd5cf23c890ecb77a7fe6')
  const room = await Room.findById('621bd5cf23c890ecb77a7feb')
  const informaticsBuilding = await Building.findById(room.building)
  console.log(newInformaticBuilding)
  console.log(room)
  console.log(informaticsBuilding)
  room.building = newInformaticBuilding
  newInformaticBuilding.room.push(room)
  informaticsBuilding.room.pull(room)
  room.save()
  newInformaticBuilding.save()
  informaticsBuilding.save()
}

main().then(() => {
  console.log('Finish')
})
